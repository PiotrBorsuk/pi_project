from time import time,sleep,strftime
from datetime import datetime
import telepot
from picamera import PiCamera
from gpiozero import AngularServo
import time

camera = PiCamera()

def handle(msg):
    global telegramText
    global chat_id
    global receiveTelegramMessage

    chat_id = msg['chat']['id']
    telegramText = msg['text']

    print('Message received from ' + str(chat_id))

    if telegramText == '/start':
        bot.sendMessage(chat_id, 'Welcome to pico bot')
    else:
        receiveTelegramMessage = True

def photo():
    camera.start_preview()
    camera.capture('/home/pi/Desktop/image.jpg')
    bot.sendPhoto(chat_id, photo=open('/home/pi/Desktop/image.jpg','rb'))
    camera.stop_preview()

def rotation(degree):
    s1 = AngularServo(21, min_angle = 0, max_angle=180, min_pulse_width=0.0006, max_pulse_width=0.0024)
    s2 = AngularServo(20, min_angle = 0, max_angle=180, min_pulse_width=0.0006, max_pulse_width=0.0024)
    if degree > 90:
        s2.angle = 110
        for a in range(90,160,2):
            s1.angle = a
            time.sleep(0.05)
    if degree < 90:
        s2.angle = 110
        for a in range(90,20,-2):
            s1.angle = a
            time.sleep(0.05)
    time.sleep(0.2)

def returning(from_where):
    s1 = AngularServo(21, min_angle = 0, max_angle=180, min_pulse_width=0.0006, max_pulse_width=0.0024)
    s2 = AngularServo(20, min_angle = 0, max_angle=180, min_pulse_width=0.0006, max_pulse_width=0.0024)
    if from_where == 'kitchen':
        s2.angle = 110
        for a in range(160,88,-2):
            s1.angle = a
            time.sleep(0.05)
    if from_where == 'salon':
        s2.angle = 110
        for a in range(20,92,2):
            s1.angle = a
            time.sleep(0.05)
    time.sleep(0.2)


bot = telepot.Bot('5660612298:AAExe8puch2-rWfcd1_uHzrYB33afd6RShM')
bot.message_loop(handle)

print('Telegram bot is ready')

receiveTelegramMessage = False
sendTelegramMessage = False

statusText = ""

kuchnia = [160,110]
hol = [90,110]
salon = [20,110]


try:
    while True:
        if receiveTelegramMessage == True:
            receiveTelegramMessage = False
            statusText = 'co'
            if telegramText == 'Kuchnia':
                rotation(160)
                photo()
                returning('kitchen')
            if telegramText == 'Salon':
                rotation(20)
                photo()
                returning('salon')
            if telegramText == 'Hol':
                photo()
                returning()
            else:
                statusText = 'Command not valid\n\n'
            sendTelegramMessage = True

except KeyboardInterrupt:
    sys.exit(0)
